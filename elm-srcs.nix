{

      "ianmackenzie/elm-units" = {
        sha256 = "0p9152w17hz8yd2vj332dq1vyzdjhr2k4navg8lqkdmy6016hkky";
        version = "2.5.1";
      };

      "elm-explorations/webgl" = {
        sha256 = "1w28j9frgqv875ff71y0383riwgaq78x87rvnaazjihky1ya6afy";
        version = "1.1.1";
      };

      "ianmackenzie/elm-3d-camera" = {
        sha256 = "1cp7ws6kn4sakzk0hpwy83qyz4mr6xfsk2jb4vbf97wjc1fs1gf7";
        version = "3.1.0";
      };

      "ianmackenzie/elm-triangular-mesh" = {
        sha256 = "01m8h9r944h7ggcs4d8f3i0x7lpq69sbng50zb4nvcq6qrma67wz";
        version = "1.0.4";
      };

      "elm-explorations/linear-algebra" = {
        sha256 = "19zf3dgrx5bvhybjwq3bp171wzsb4bfk5rrfjv234b2f8q3yi59g";
        version = "1.0.3";
      };

      "ianmackenzie/elm-1d-parameter" = {
        sha256 = "08r1q80k7w2kyasqk54k1xzd5d1607wj4ahx66jdh7gkrni9m8n5";
        version = "1.0.1";
      };

      "elm/html" = {
        sha256 = "1n3gpzmpqqdsldys4ipgyl1zacn0kbpc3g4v3hdpiyfjlgh8bf3k";
        version = "1.0.0";
      };

      "ianmackenzie/elm-geometry-linear-algebra-interop" = {
        sha256 = "15gjd1h0jv3d35p0hyv4k8z1gpag23dip8yacclajjrnkdxmcpgj";
        version = "2.0.2";
      };

      "ianmackenzie/elm-float-extra" = {
        sha256 = "0l499d14iabyigdysvx3xa572izmqqki7b1ym2gc7kv4sprrq194";
        version = "1.1.0";
      };

      "elm/browser" = {
        sha256 = "0nagb9ajacxbbg985r4k9h0jadqpp0gp84nm94kcgbr5sf8i9x13";
        version = "1.0.2";
      };

      "avh4/elm-color" = {
        sha256 = "0n16wnvp87x9az3m5qjrl6smsg7051m719xn5d244painx8xmpzq";
        version = "1.0.0";
      };

      "elm/core" = {
        sha256 = "19w0iisdd66ywjayyga4kv2p1v9rxzqjaxhckp8ni6n8i0fb2dvf";
        version = "1.0.5";
      };

      "ianmackenzie/elm-geometry" = {
        sha256 = "0lrrhbsi4zlj4hpmbimglrfb00p0fngmb8vhdxm70f8gyp3sg5f8";
        version = "3.6.0";
      };

      "elm/time" = {
        sha256 = "0vch7i86vn0x8b850w1p69vplll1bnbkp8s383z7pinyg94cm2z1";
        version = "1.0.0";
      };

      "elm/json" = {
        sha256 = "0kjwrz195z84kwywaxhhlnpl3p251qlbm5iz6byd6jky2crmyqyh";
        version = "1.1.3";
      };

      "ianmackenzie/elm-interval" = {
        sha256 = "1whj4w89g01ixjiijxk93f753ql64ny213dr9hqnxig12c2py1qa";
        version = "2.0.0";
      };

      "ianmackenzie/elm-units-interval" = {
        sha256 = "1b1rajh3b0a2xpfngang1d83ia2lrlqqbylnw544wibywzz4phcv";
        version = "1.1.0";
      };

      "elm/url" = {
        sha256 = "0av8x5syid40sgpl5vd7pry2rq0q4pga28b4yykn9gd9v12rs3l4";
        version = "1.0.0";
      };

      "elm/virtual-dom" = {
        sha256 = "0q1v5gi4g336bzz1lgwpn5b1639lrn63d8y6k6pimcyismp2i1yg";
        version = "1.0.2";
      };
}
