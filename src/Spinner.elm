module Spinner exposing (..)

import Angle
import Axis3d
import Block3d
import Browser exposing (Document)
import Camera3d
import Color
import Direction3d
import Frame3d
import Html
import Length
import Pixels
import Point3d
import Quantity
import Scene3d
import Scene3d.Material as Material
import Size
import Time
import Viewpoint3d


main =
    Browser.document
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type alias Model =
    { cube : Entity
    , rotationSpeed : Angle.Angle
    , windowSize : Size.Size
    }


type Msg
    = Tick Time.Posix
    | Resize Size.Size


type ZUpCoordinates
    = ZUpCoordinates


type alias Cube =
    Block3d.Block3d Length.Meters ZUpCoordinates


type alias Entity =
    Scene3d.Entity ZUpCoordinates


init : () -> ( Model, Cmd Msg )
init _ =
    ( { cube = cubeEntity cubeBlock
      , rotationSpeed = Angle.radians <| (pi / 1024)
      , windowSize = { width = 500, height = 500 }
      }
    , Size.getWindowSize Resize
    )


view : Model -> Document Msg
view model =
    { title = "Plate"
    , body = [ viewScene model ]
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick time ->
            ( { model
                | cube = rotateCube model
              }
            , Cmd.none
            )

        Resize size ->
            ( { model | windowSize = size }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    -- 16ms is circa 60Hz
    Sub.batch
        [ Time.every 16 Tick
        , Size.subWindowSize Resize
        ]



{- 3D STUFF -}


upDirection : Direction3d.Direction3d coordinates
upDirection =
    Direction3d.positiveZ



{- CUBE -}


cubeBlock : Cube
cubeBlock =
    -- make cube rotated in pretty way
    let
        p1 =
            Point3d.meters -1 -1 -1

        p2 =
            Point3d.meters 1 1 1
    in
    Block3d.from p1 p2
        |> Block3d.rotateAround Axis3d.z
            (Angle.radians <| pi / 4)
        |> Block3d.rotateAround Axis3d.x
            -- this is sort of magic, don't worry about it
            -- v = sin(l_a / l_b)
            (Angle.radians <| 0.9877659459927355)


cubeEntity : Cube -> Entity
cubeEntity cube =
    Scene3d.block (Material.matte Color.darkCharcoal) cube


rotateCube : Model -> Entity
rotateCube model =
    model.cube
        |> Scene3d.rotateAround Axis3d.z
            (Angle.radians (Angle.inRadians model.rotationSpeed))



{- RENDERING -}


camera : Camera3d.Camera3d Length.Meters ZUpCoordinates
camera =
    Camera3d.perspective
        { viewpoint =
            Viewpoint3d.lookAt
                { eyePoint = Point3d.meters 0 -10 0
                , focalPoint = Point3d.origin
                , upDirection = upDirection
                }
        , verticalFieldOfView = Angle.degrees 30
        }


viewScene : Model -> Html.Html Msg
viewScene model =
    Scene3d.cloudy
        { entities = [ model.cube ]
        , camera = camera
        , clipDepth = Length.meters 1

        -- this is super fucked up
        -- for some reason, this results in qutebrowser going transparent
        -- and displaying my wallpaper as the background of this window
        , background = Scene3d.backgroundColor Color.blue

        -- TODO: dimensions should be window size
        , dimensions =
            ( Pixels.pixels model.windowSize.width
            , Pixels.pixels model.windowSize.height
            )
        , upDirection = upDirection
        }
