module Size exposing (Size, getWindowSize, subWindowSize)

import Browser.Dom
import Browser.Events
import Task



{- SIZE
   These are functions used to get window/viewport size.
-}


type alias Size =
    { height : Float
    , width : Float
    }


subWindowSize : (Size -> msg) -> Sub msg
subWindowSize msg =
    Browser.Events.onResize
        (\w h -> msg { width = toFloat w, height = toFloat h })


getWindowSize : (Size -> msg) -> Cmd msg
getWindowSize msg =
    -- get the viewport size sent back as a WindowSizeMsg
    Task.perform
        (\{ viewport } ->
            msg
                { width = viewport.width
                , height = viewport.height
                }
        )
        Browser.Dom.getViewport
