
# Spinner startpage

Small startpage written in Elm, that uses `elm-3d-scene` to render and rotate a 3d cube.


## How to use

After cloning submodules, you can build the startpage using the nix package manager.

`nix-build` should be sufficient.

(Of course the plain elm tooling still works as well.)


## Stuff that I'd like to do:

- Set colors at compile time
- Clone elm-3d-scene submodule using nix expresions
- Add a screenshot here
- Include elm-3d-scene as a regular elm package (once it is released).
